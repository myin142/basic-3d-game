extends Spatial

const MOVEMENT_SCENE = "movement/movement.tscn"
const AIMING_SCENE = "aiming/aiming.tscn"

func _ready():
	_change_scene(AIMING_SCENE)

func _process(delta):
	if Input.is_action_just_pressed("movement_test"):
		_change_scene(MOVEMENT_SCENE)
	
	if Input.is_action_just_pressed("aiming_test"):
		_change_scene(AIMING_SCENE)

func _change_scene(scene_path):
	get_tree().change_scene("res://scenes/" + scene_path)
