## Basic 3D Game in Godot
I use this project to test 3d code in godot.

### Basic Usage
When you clone this project you will probably be missing the model files in godot.
Follow the steps in the [blender import](./docs/blender-import.md) to export all the blender model
to a `.gltf` file.

### Software
 - Blender v2.82a
 - Godot v3.2.1