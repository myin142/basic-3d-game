extends AnimationTree

const MOVEMENT_BLEND = "parameters/move/blend_amount"
const STRAFE_BLEND = "parameters/strafe/blend_position"
const CURRENT_STATE = "parameters/state/current"
const MOVE_STATE = 0
const MID_AIR_STATE = 1
const RUNNING_STATE = 2
const STRAFE_STATE = 3

func mid_air():
	_set_current_state(MID_AIR_STATE)
	
func running():
	_set_current_state(RUNNING_STATE)

func move(blend):
	_set_current_state(MOVE_STATE)
	set(MOVEMENT_BLEND, blend)

func strafe(blend):
	_set_current_state(STRAFE_STATE)
	set(STRAFE_BLEND, blend)
	
func _set_current_state(state):
	set(CURRENT_STATE, state)
