extends KinematicBody

export var motion_interpolate_speed = 10
export var rotation_interpolate_speed = 10

export var jump_speed = 5
export var max_floor_angle = 50

onready var animation_tree = $AnimationTree
onready var camera = $CameraBase
onready var player = $player

onready var gravity = ProjectSettings.get_setting("physics/3d/default_gravity") * ProjectSettings.get_setting("physics/3d/default_gravity_vector")

var orientation = Transform()
var root_motion = Transform()
var motion = Vector2()
var velocity = Vector3()
var apply_rootmotion = true

func _init():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _ready():
	orientation = player.global_transform
	orientation.origin = Vector3()

func _physics_process(delta):
	player.update_states(is_on_floor())
	motion = motion.linear_interpolate(player.get_motion_target(), motion_interpolate_speed * delta)

	if player.is_not_grounded_floor():
		move_and_collide(Vector3.DOWN)
		
	if is_on_floor():
		apply_rootmotion = true

	if player.jumped():
		velocity.y = jump_speed
#
	if not player.is_grounded():
		animation_tree.mid_air()
	elif player.is_aiming():
		_rotate_to(camera.global_transform, delta)
		animation_tree.strafe(Vector2(motion.x, -motion.y))
		root_motion = animation_tree.get_root_motion_transform()
	else:
		var target = camera.target_direction_for_motion(motion)
		if target.length() > 0.01:
			_rotate_to_target(target, delta)

		if player.is_running():
			animation_tree.running()
		else:
			animation_tree.move(motion.length())
	
		root_motion = animation_tree.get_root_motion_transform()

	if apply_rootmotion:
		orientation *= root_motion
	
	var h_velocity = orientation.origin / delta
	velocity.x = h_velocity.x
	velocity.z = h_velocity.z
	velocity += gravity * delta
	velocity = move_and_slide(velocity, Vector3.UP, true, 4, deg2rad(max_floor_angle))
	
	var floor_angle = _collision_angle(0)
	if floor_angle > max_floor_angle:
		# fall faster on an angled wall
		velocity += gravity * ((90 - floor_angle) / 90)
		
		# Disable root motion to prevent player from moving up an angled wall
		apply_rootmotion = false
	
	orientation.origin = Vector3()
	orientation = orientation.orthonormalized()
	
	player.global_transform.basis = orientation.basis

func _rotate_to_target(target, delta):
	_rotate_to(Transform().looking_at(target, Vector3.UP), delta)

func _rotate_to(to, delta):
	var q_from = orientation.basis.get_rotation_quat()
	var q_to = to.basis.get_rotation_quat()
	orientation.basis = Basis(q_from.slerp(q_to, delta * rotation_interpolate_speed))

func _collision_angle(defaultValue):
	var count = get_slide_count()
	if count > 0:
		var collision = get_slide_collision(0)
		var normal = collision.get_normal()
		return rad2deg(acos(normal.dot(Vector3.UP)))
	
	return defaultValue
