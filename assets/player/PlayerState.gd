extends Spatial

onready var ground_ray = $"../GroundRayCast"

# Interal state variables
var _is_grounded = true
var _is_jumping = false
var _is_on_floor = true
var _is_moving = false

# Call this function inside processing function
# to update all states for current frame
func update_states(on_floor):
	_is_grounded = on_floor or ground_ray.is_colliding()
	_is_on_floor = on_floor
	_is_moving = get_motion_target().length() > 0
	
	if _is_on_floor:
		_is_jumping = false

func is_not_grounded_floor():
	return _is_grounded and not _is_on_floor and not _is_jumping

func get_motion_target():
	return Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_backward") - Input.get_action_strength("move_forward")
	)

func is_grounded():
	return _is_grounded

func is_running():
	return _is_moving and Input.is_action_pressed("run")
	
func is_aiming():
	return Input.is_action_pressed("aim")

func just_aimed():
	return Input.is_action_just_pressed("aim")
	
func just_released_aim():
	return Input.is_action_just_released("aim")

func jumped():
	var has_jumped = _is_grounded and Input.is_action_just_pressed("jump")
	if has_jumped:
		_is_jumping = true

	return has_jumped
	
